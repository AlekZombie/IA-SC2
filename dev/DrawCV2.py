import cv2
import numpy as np
from sc2.constants import UnitTypeId


class DrawIntel:

    def __init__(self):
        # draw dict for ours units_type
        self.own_draw_dict = {

        }
        # draw dict for enemy units_type

    async def draw_map_game(self):
        game_data = np.zeros((self.game_info.map_size[1], self.game_info.map_size[0], 1), np.uint8)

        draw_dict = {
            UnitTypeId.HATCHERY: [15, (0, 255, 0)],
            UnitTypeId.OVERLORD: [2, (20, 255, 0)],
            UnitTypeId.DRONE: [2, (55, 255, 0)],
            UnitTypeId.ZERGLING: [2, (110, 255, 55)],

            UnitTypeId.NEXUS: [15, (255, 0, 255)]

        }

        for unit_type in draw_dict:
            for unit in self.units(unit_type).ready:
                pos = unit.position
                cv2.circle(game_data, (int(pos[0]), int(pos[1])), draw_dict[unit_type][0],
                           draw_dict[unit_type][1], -1)

        for mineral in self.state.mineral_field:
            min_pos = mineral.position
            cv2.circle(game_data, (int(min_pos[0]), (int(min_pos[1]))), 1, (0, 0, 255), -1)

        flipped = cv2.flip(game_data, 0)
        resized = cv2.resize(flipped, dsize=None, fx=2, fy=2)
        cv2.imshow('Intel', resized)
        cv2.waitKey(1)
