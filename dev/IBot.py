from abc import abstractmethod
from DrawCV2 import DrawIntel


class IBotZerg(DrawIntel):

    def __init__(self):
        super().__init__()
        # here you can add your variables, for the bot
        self.MAX_DRONES = 60
        self.count_of_zerlings = 0

    @abstractmethod
    async def update_data(self): raise NotImplementedError

    # <editor-fold desc="Chat">
    @abstractmethod
    async def chat(self, iteration): raise NotImplementedError
    # </editor-fold>

    # <editor-fold desc="Buildings">

    # <editor-fold desc="Hatchery section">

    @abstractmethod
    async def build_extractor(self): raise NotImplementedError

    @abstractmethod
    async def build_spawning_pool(self): raise NotImplementedError

    @abstractmethod
    async def build_evolution_chamber(self): raise NotImplementedError

    @abstractmethod
    async def build_spore_crawler(self): raise NotImplementedError

    @abstractmethod
    async def build_roach_warren(self): raise NotImplementedError

    @abstractmethod
    async def build_baneling_nest(self): raise NotImplementedError

    @abstractmethod
    async def build_spine_crawler(self): raise NotImplementedError

    # </editor-fold>
    # <editor-fold desc="Lair section">

    @abstractmethod
    async def upgrade_to_lair(self): raise NotImplementedError

    @abstractmethod
    async def build_hydralisk_den(self): raise NotImplementedError

    @abstractmethod
    async def build_infestation_pit(self): raise NotImplementedError

    @abstractmethod
    async def build_spire(self): raise NotImplementedError

    @abstractmethod
    async def build_nydus_network(self): raise NotImplementedError

    # </editor-fold>
    # <editor-fold desc="Hive section">

    async def upgrade_to_hive(self): raise NotImplementedError

    async def build_ultralisk_cavern(self): raise NotImplementedError

    async def build_greater_spire(self): raise NotImplementedError

    # </editor-fold>

    @abstractmethod
    async def expand(self): raise NotImplementedError
    # </editor-fold>

    # <editor-fold desc="Mobs">
    @abstractmethod
    async def raise_queen(self): raise NotImplementedError

    @abstractmethod
    async def raise_drones(self): raise NotImplementedError

    @abstractmethod
    async def raise_overlord(self): raise NotImplementedError

    @abstractmethod
    async def raise_zerglings(self): raise NotImplementedError
    # </editor-fold>

    # <editor-fold desc="Upgrades for Mobs">

    @abstractmethod
    async def inv_pcarapace(self): raise NotImplementedError

    @abstractmethod
    async def inv_burrow(self): raise NotImplementedError

    @abstractmethod
    async def inv_metabolic_boost(self): raise NotImplementedError

    @abstractmethod
    async def inv_aglands(self): raise NotImplementedError

    @abstractmethod
    async def inv_melee_attacks_1(self): raise NotImplementedError

    @abstractmethod
    async def inv_melee_attacks_2(self): raise NotImplementedError

    @abstractmethod
    async def inv_melee_attacks_3(self): raise NotImplementedError

    @abstractmethod
    async def inv_missile_attacks_1(self): raise NotImplementedError

    @abstractmethod
    async def inv_missile_attacks_2(self): raise NotImplementedError

    @abstractmethod
    async def inv_missile_attacks_3(self): raise NotImplementedError

    @abstractmethod
    async def inv_ground_carapace_1(self): raise NotImplementedError

    @abstractmethod
    async def inv_ground_carapace_2(self): raise NotImplementedError

    @abstractmethod
    async def inv_ground_carapace_3(self): raise NotImplementedError

    @abstractmethod
    async def inv_tunneling_claws(self): raise NotImplementedError

    @abstractmethod
    async def inv_glial_reconstitution(self): raise NotImplementedError

    @abstractmethod
    async def inv_centrifugal_hooks(self): raise NotImplementedError

    # </editor-fold>

    # <editor-fold desc="Micro for mobs">
    @abstractmethod
    async def assign_workers_to_extractors(self): raise NotImplementedError

    @abstractmethod
    async def defense_with_zerglings(self): raise NotImplementedError

    @abstractmethod
    async def move_overlords(self): raise NotImplementedError

    @abstractmethod
    async def scouting(self): raise NotImplementedError

    # </editor-fold>