# imports about SC2
from sc2 import BotAI
from sc2.constants import UnitTypeId, UpgradeId, AbilityId
from IBot import *
import random

# TODO
# - Scouting, looking for the enemy and set a perimeter for defences(?)
# - A way to obtain the data for statistics of this game
# - A way to obtain the data for IA(?)


class AlekZergBot(BotAI, IBotZerg):

    def __init__(self):
        super().__init__()
        self.mboost_ready = False
        self.mboost_started = False
        self.count_of_zerlings = 0

    async def on_step(self, iteration):
        print(self.time)

        await self.scouting()
        await self.draw_map_game()

        # updating data
        self.update_data()

        await self.distribute_workers()
        # sending glhf and visit me msg
        await self.chat(iteration)
        await self.move_overlords()

        await self.raise_overlord()
        await self.raise_drones()
        await self.build_extractor()
        await self.raise_queen()
        await self.assign_workers_to_extractors()
        await self.build_spawning_pool()
        # await self.raise_zerglings()
        await self.upgrade_to_lair()
        await self.build_hydralisk_den()
        # await self.inv_metabolic_boost()
        await self.expand()
        # await self.defense_with_zerglings()

    def update_data(self):
        self.count_of_zerlings = self.units(UnitTypeId.ZERGLING).amount

    async def chat(self, iteration):
        if iteration == 0:
            await self.chat_send("glhf")
            await self.chat_send("please visit me at alekzombie.gitlab.io")

    async def build_extractor(self):
        # if we don't have at least 2 extractor for hatchery, try to build more in the proximity
        for townhall in self.townhalls:
            for vespene in self.state.vespene_geyser.closer_than(10, townhall):
                if not self.can_afford(UnitTypeId.EXTRACTOR):
                    break
                else:
                    worker = self.workers.random
                await self.do(worker.build(UnitTypeId.EXTRACTOR, vespene))

    async def build_spawning_pool(self):
        hq = self.townhalls.first
        if not (self.units(UnitTypeId.SPAWNINGPOOL).exists or self.already_pending(UnitTypeId.SPAWNINGPOOL)):
            if self.can_afford(UnitTypeId.SPAWNINGPOOL):
                await self.build(UnitTypeId.SPAWNINGPOOL, near=hq.add_on_land_position)

    async def build_evolution_chamber(self):
        pass

    async def build_spore_crawler(self):
        pass

    async def build_roach_warren(self):
        pass

    async def build_baneling_nest(self):
        pass

    async def build_spine_crawler(self):
        pass

    async def upgrade_to_lair(self):
        hq = self.townhalls.first
        if self.units(UnitTypeId.SPAWNINGPOOL).exists:
            if not self.units(UnitTypeId.LAIR).exists \
                    and hq.noqueue:
                if self.minerals > 350 \
                        and self.vespene > 250:
                    if self.can_afford(UnitTypeId.LAIR):
                        await self.do(hq.build(UnitTypeId.LAIR))

    async def build_hydralisk_den(self):
        townhall = self.townhalls.first
        if self.units(UnitTypeId.LAIR).ready.exists \
                and not self.units(UnitTypeId.HYDRALISKDEN).ready.exists:
            if self.can_afford(UnitTypeId.HYDRALISKDEN) \
                    and not self.already_pending(UnitTypeId.HYDRALISKDEN):
                await self.build(UnitTypeId.HYDRALISKDEN, near=townhall.position, max_distance=30)

    async def build_infestation_pit(self):
        pass

    async def build_spire(self):
        pass

    async def build_nydus_network(self):
        pass

    async def upgrade_to_hive(self):
        pass

    async def build_ultralisk_cavern(self):
        pass

    async def build_greater_spire(self):
        pass

    async def expand(self):
        if self.townhalls.amount < 3 and self:
            if self.can_afford(UnitTypeId.HATCHERY):
                await self.expand_now()

    async def raise_drones(self):
        for townhall in self.townhalls:
            if townhall.assigned_harvesters < townhall.ideal_harvesters:
                # getting the larvae of the townhall
                closer_larvae = self.units(UnitTypeId.LARVA).closer_than(5, townhall)
                if self.can_afford(UnitTypeId.DRONE) \
                        and closer_larvae.exists \
                        and self.units(UnitTypeId.DRONE).amount < self.MAX_DRONES:
                        # and not self.al(UnitTypeId.DRONE) \
                    await self.do(closer_larvae.first.train(UnitTypeId.DRONE))
                else:
                    break
            else:
                break

    async def raise_overlord(self):
        larvae = self.units(UnitTypeId.LARVA)
        if self.supply_left <= 2 \
                and not self.already_pending(UnitTypeId.OVERLORD):
            if self.can_afford(UnitTypeId.OVERLORD) \
                    and larvae.exists:
                await self.do(larvae.random.train(UnitTypeId.OVERLORD))

    async def raise_zerglings(self):
        if self.units(UnitTypeId.SPAWNINGPOOL).ready \
                and self.count_of_zerlings <= 20:
            for townhall in self.townhalls:
                closer_larvae = self.units(UnitTypeId.LARVA).closer_than(5, townhall)
                if self.can_afford(UnitTypeId.ZERGLING) \
                        and closer_larvae.exists:
                    await self.do(closer_larvae.train(UnitTypeId.ZERGLING))
                else:
                    break

    async def raise_queen(self):
        if self.units(UnitTypeId.SPAWNINGPOOL).exists:
            townhall = self.townhalls.first
            if townhall.noqueue \
                    and not self.units(UnitTypeId.QUEEN).closer_than(60, townhall.position).exists \
                    and townhall.noqueue:
                if self.can_afford(UnitTypeId.QUEEN):
                    await self.do(townhall.train(UnitTypeId.QUEEN))

    async def inv_pcarapace(self):
        pass

    async def inv_burrow(self):
        pass

    async def inv_metabolic_boost(self):
        sp = self.units(UnitTypeId.SPAWNINGPOOL).ready

        if sp.noqueue and self.mboost_started:
            self.mboost_ready = True
        else:
            if sp.exists and self.minerals > 100 and not self.mboost_started:
                await self.do(sp.first(UpgradeId.RESEARCH_ZERGLINGMETABOLICBOOST))
                self.mboost_started = True

    async def inv_aglands(self):
        pass

    async def inv_melee_attacks_1(self):
        pass

    async def inv_melee_attacks_2(self):
        pass

    async def inv_melee_attacks_3(self):
        pass

    async def inv_missile_attacks_1(self):
        pass

    async def inv_missile_attacks_2(self):
        pass

    async def inv_missile_attacks_3(self):
        pass

    async def inv_ground_carapace_1(self):
        pass

    async def inv_ground_carapace_2(self):
        pass

    async def inv_ground_carapace_3(self):
        pass

    async def inv_tunneling_claws(self):
        pass

    async def inv_glial_reconstitution(self):
        pass

    async def inv_centrifugal_hooks(self):
        pass

    async def defense_with_zerglings(self):
        if self.mboost_ready \
                and self.units(UnitTypeId.ZERGLING).amount > 12:
            for zergling in self.units(UnitTypeId.ZERGLING).idle:
                if self.known_enemy_units.exists:
                    await self.do(zergling.attack(random.choice(self.known_enemy_units)))

    async def move_overlords(self):
        townhalls = self.townhalls
        for townhall in townhalls:
            for overlord in self.units(UnitTypeId.OVERLORD).closer_than(20, townhall.position):
                if overlord.is_idle:
                    # why random, cuz every non idle overlord will eventually follow the same DRONE!
                    await self.do(overlord.move(self.units(UnitTypeId.DRONE).ready.random.position))

    async def assign_workers_to_extractors(self):
        for extractor in self.units(UnitTypeId.EXTRACTOR).ready:
            if extractor.assigned_harvesters < extractor.ideal_harvesters:
                worker = self.workers.closer_than(20, extractor)
                if worker.exists:
                    await self.do(worker.random.gather(extractor))
            else:
                break

    async def scouting(self):
        worker = self.units(UnitTypeId.DRONE)[0]
        if worker.is_idle:
            enemy_location = self.enemy_start_locations[0]
            await self.do(worker.move(enemy_location))
